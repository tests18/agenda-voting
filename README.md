# Agenda Voting -  Sicredi Test

## Getting Start
For the application to work must start the docker so that the application may persist data on the database.
```sh
> docker-compose up -d
```
Make sure that there is no application occupying port 5432.

----------

## Api Documentation
- [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
