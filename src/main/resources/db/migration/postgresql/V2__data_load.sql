SET client_encoding = 'UTF8';
SET TIME ZONE 'America/Sao_Paulo';

INSERT INTO associate(id, name, document) VALUES
    (1, 'John Doe', '85661986033'),
    (2, 'Jane Doe', '67567615010'),
    (3, 'Rebecca G. Harmon', '29945344013');