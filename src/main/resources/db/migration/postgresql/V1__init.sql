SET client_encoding = 'UTF8';
SET TIME ZONE 'America/Sao_Paulo';

CREATE SEQUENCE agenda_seq;

CREATE TABLE agenda
(
    id BIGINT NOT NULL,
    title VARCHAR(150) NOT NULL,
    description VARCHAR(255),
    created_at TIMESTAMP NOT NULL,
    begin_session_at TIMESTAMP,
    end_session_at TIMESTAMP,
    CONSTRAINT agenda_pkey PRIMARY KEY (id)
);


CREATE SEQUENCE associate_seq;

CREATE TABLE associate (
  id BIGINT NOT NULL,
  name VARCHAR(150) NOT NULL,
  document VARCHAR(150) NOT NULL,
  CONSTRAINT associate_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE vote_seq;

CREATE TABLE vote (
    id BIGINT NOT NULL,
    associate_id BIGINT NOT NULL,
    option BOOLEAN NOT NULL,
    created_at TIMESTAMP NOT NULL,
    agenda_id BIGINT NOT NULL,
    CONSTRAINT vote_pkey PRIMARY KEY (id),
    FOREIGN KEY (agenda_id) REFERENCES agenda(id),
    FOREIGN KEY (associate_id) REFERENCES associate(id)
)