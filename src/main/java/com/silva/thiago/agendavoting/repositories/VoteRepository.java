package com.silva.thiago.agendavoting.repositories;

import com.silva.thiago.agendavoting.models.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteRepository extends JpaRepository<Vote, Long> {
    Boolean existsByAssociateIdAndAgendaId(Long associateId, Long agendaId);
}
