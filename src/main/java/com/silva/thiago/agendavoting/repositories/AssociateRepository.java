package com.silva.thiago.agendavoting.repositories;

import com.silva.thiago.agendavoting.models.Associate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssociateRepository extends JpaRepository<Associate, Long> {
}
