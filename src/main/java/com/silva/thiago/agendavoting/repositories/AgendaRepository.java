package com.silva.thiago.agendavoting.repositories;

import com.silva.thiago.agendavoting.models.Agenda;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgendaRepository extends JpaRepository<Agenda, Long> {
}
