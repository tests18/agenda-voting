package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.CreateVoteDTO;
import com.silva.thiago.agendavoting.models.Agenda;
import com.silva.thiago.agendavoting.models.Associate;
import com.silva.thiago.agendavoting.models.Vote;
import com.silva.thiago.agendavoting.repositories.VoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class VoteServiceImpl implements VoteService {
    private VoteRepository voteRepository;
    private AssociateService associateService;
    private DocumentCheckService documentCheckService;

    @Override
    public void vote(Agenda agenda, CreateVoteDTO createVoteDTO) {
        Associate associate = associateService.findById(createVoteDTO.getAssociateId());

        if (!agenda.hasOpenSession()) {
            throw new DataIntegrityViolationException("Agenda must be initialized");
        }

        if (voteRepository.existsByAssociateIdAndAgendaId(associate.getId(), agenda.getId())) {
            throw new DataIntegrityViolationException("Associate already voted for this agenda");
        }

        if (!documentCheckService.isAbleToVote(associate.getDocument())) {
            throw new DataIntegrityViolationException("Associate unable to vote");
        }

        var vote = Vote
            .builder()
            .agenda(agenda)
            .associate(associate)
            .option(createVoteDTO.getOption())
            .build();

        voteRepository.save(vote);
    }
}
