package com.silva.thiago.agendavoting.services;

public interface DocumentCheckService {
    Boolean isAbleToVote(String document);
}
