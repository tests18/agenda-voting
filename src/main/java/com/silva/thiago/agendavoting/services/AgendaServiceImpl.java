package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.AgendaDTO;
import com.silva.thiago.agendavoting.dtos.AgendaResultDTO;
import com.silva.thiago.agendavoting.dtos.CreateAgendaDTO;
import com.silva.thiago.agendavoting.models.Agenda;
import com.silva.thiago.agendavoting.models.Vote;
import com.silva.thiago.agendavoting.repositories.AgendaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AgendaServiceImpl implements AgendaService {
    private final AgendaRepository agendaRepository;
    private final ModelMapper modelMapper;

    public AgendaServiceImpl(AgendaRepository agendaRepository, ModelMapper modelMapper) {
        this.agendaRepository = agendaRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Agenda findById(Long id) {
        return this.agendaRepository.getOne(id);
    }

    @Override
    public AgendaDTO insert(CreateAgendaDTO createAgendaDTO) {
        var agenda = modelMapper.map(createAgendaDTO, Agenda.class);
        return modelMapper.map(agendaRepository.save(agenda), AgendaDTO.class);
    }

    @Override
    public List<AgendaDTO> findAll() {
        return this.agendaRepository
            .findAll()
            .stream()
            .map(entity -> modelMapper.map(entity, AgendaDTO.class))
            .collect(Collectors.toList());
    }

    @Override
    public void start(Long agendaId, Optional<LocalDateTime> endAt) {
        Agenda agenda = agendaRepository.getOne(agendaId);

        if (agenda.hasOpenSession()) {
            throw new DataIntegrityViolationException(String.format("Agenda %d already initialized", agendaId));
        }

        agenda.setBeginSessionAt(LocalDateTime.now());
        agenda.setEndSessionAt(endAt.orElse(defaultSessionEnd()));

        agendaRepository.save(agenda);
    }

    @Override
    public AgendaResultDTO result(Long id) {
        var agenda = findById(id);
        Integer totalVotes = agenda.getVoteList().size();
        Map<Boolean, Long> result = agenda
            .getVoteList()
            .stream()
            .collect(Collectors.groupingBy(Vote::getOption, Collectors.counting()));


        return AgendaResultDTO
            .builder()
            .agendaId(agenda.getId())
            .agendaTitle(agenda.getTitle())
            .result(result)
            .percentage(
                result
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue() * 100 / totalVotes
                    ))
            )
            .build();
    }

    private LocalDateTime defaultSessionEnd() {
        return LocalDateTime.now().plusMinutes(1L);
    }
}
