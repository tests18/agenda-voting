package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.AssociateDTO;
import com.silva.thiago.agendavoting.models.Associate;
import com.silva.thiago.agendavoting.repositories.AssociateRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AssociateServiceImpl implements AssociateService {
    private AssociateRepository associateRepository;
    private ModelMapper modelMapper;

    public AssociateServiceImpl(AssociateRepository associateRepository, ModelMapper modelMapper) {
        this.associateRepository = associateRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Associate findById(Long id) {
        return associateRepository.getOne(id);
    }

    @Override
    public List<AssociateDTO> findAll() {
        return associateRepository.findAll()
            .stream()
            .map(associate -> modelMapper.map(associate, AssociateDTO.class))
            .collect(Collectors.toList());
    }
}
