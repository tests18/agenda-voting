package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.DocumentStatusDTO;
import com.silva.thiago.agendavoting.enums.DocumentStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class DocumentCheckServiceImpl implements DocumentCheckService {
    private static final String URL = "https://user-info.herokuapp.com/users/%s";
    private RestTemplate restTemplate;

    @Autowired
    public DocumentCheckServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Boolean isAbleToVote(String document) {
        DocumentStatusDTO associateDocumentStatusDTO = restTemplate.getForObject(
                String.format(this.URL, document),
                DocumentStatusDTO.class
        );

        return associateDocumentStatusDTO.getStatus().equals(DocumentStatusEnum.ABLE_TO_VOTE);
    }
}
