package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.AgendaDTO;
import com.silva.thiago.agendavoting.dtos.AgendaResultDTO;
import com.silva.thiago.agendavoting.dtos.CreateAgendaDTO;
import com.silva.thiago.agendavoting.models.Agenda;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface AgendaService {
    Agenda findById(Long id);

    List<AgendaDTO> findAll();

    AgendaDTO insert(CreateAgendaDTO createAgendaDTO);

    void start(Long agendaId, Optional<LocalDateTime> endAt);

    AgendaResultDTO result(Long id);
}
