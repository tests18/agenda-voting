package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.AssociateDTO;
import com.silva.thiago.agendavoting.models.Associate;

import java.util.List;

public interface AssociateService {
    Associate findById(Long id);

    List<AssociateDTO> findAll();
}
