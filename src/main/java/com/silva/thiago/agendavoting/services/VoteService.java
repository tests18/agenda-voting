package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.CreateVoteDTO;
import com.silva.thiago.agendavoting.models.Agenda;

public interface VoteService {
    void vote(Agenda agenda, CreateVoteDTO createVoteDTO);
}
