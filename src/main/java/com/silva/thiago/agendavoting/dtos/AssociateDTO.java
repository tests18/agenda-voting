package com.silva.thiago.agendavoting.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssociateDTO {
    private Long id;
    private String name;
    private String document;
}
