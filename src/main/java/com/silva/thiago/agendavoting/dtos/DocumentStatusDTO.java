package com.silva.thiago.agendavoting.dtos;

import com.silva.thiago.agendavoting.enums.DocumentStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentStatusDTO {
    private DocumentStatusEnum status;
}
