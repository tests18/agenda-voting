package com.silva.thiago.agendavoting.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AgendaResultDTO {
    private Long agendaId;

    private String agendaTitle;

    private Map<Boolean, Long> result = new HashMap<>();

    private Map<Boolean, Long> percentage = new HashMap<>();

}
