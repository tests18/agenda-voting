package com.silva.thiago.agendavoting.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class ErrorResponseDTO {
    private Integer code;
    private String message;
    private Map<String, String> errors;
}
