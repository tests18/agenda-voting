package com.silva.thiago.agendavoting.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAgendaDTO {
    @NotBlank(message = "Title cannot be blank")
    private String title;
    private String description;
}
