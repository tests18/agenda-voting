package com.silva.thiago.agendavoting.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateVoteDTO {
    @NotNull(message = "Associate id cannot be null")
    private Long associateId;
    @NotNull(message = "Option cannot be null")
    private Boolean option;
}
