package com.silva.thiago.agendavoting.controllers;

import com.silva.thiago.agendavoting.dtos.*;
import com.silva.thiago.agendavoting.services.AgendaService;
import com.silva.thiago.agendavoting.services.VoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Api(value = "Api Para gerenciamento de pautas")
@CrossOrigin(origins = "*")
@RequestMapping(AgendaController.BASE_URL)
public class AgendaController {
    public static final String BASE_URL = "/api/agenda";
    private final AgendaService agendaService;
    private final VoteService voteService;

    @Autowired
    public AgendaController(AgendaService agendaService, VoteService voteService) {
        this.agendaService = agendaService;
        this.voteService = voteService;
    }

    @GetMapping
    @ApiOperation("Lista as pautas")
    public ResponseEntity<List<AgendaDTO>> listAgenda() {
        return ResponseEntity.ok(agendaService.findAll());
    }

    @PostMapping
    @ApiOperation("Inserir nova pauta")
    public ResponseEntity<AgendaDTO> create(@Valid @RequestBody CreateAgendaDTO createAgendaDTO) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(agendaService.insert(createAgendaDTO));
    }

    @PostMapping("/{id}/vote")
    @ApiOperation("Adiciona um voto a pauta")
    public ResponseEntity vote(
        @ApiParam(value = "Id da pauta") @PathVariable Long id,
        @Valid @RequestBody CreateVoteDTO createVoteDTO
    ) {
        voteService.vote(
            agendaService.findById(id),
            createVoteDTO
        );
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/result")
    @ApiOperation("Exibe resultado das votações")
    public ResponseEntity<AgendaResultDTO> result(@ApiParam(value = "Id da pauta") @PathVariable Long id) {
        return ResponseEntity.ok(agendaService.result(id));
    }

    @PatchMapping("/{id}/start")
    @ApiOperation("Inicia nova sessão de votos para pauta com")

    public ResponseEntity startDefaultSession(
        @ApiParam(value = "Id da pauta") @PathVariable Long id,
        @ApiParam(value = "Data/hora fim da sessão opcional") @Valid @RequestBody(required = false) StartSessionDTO startSessionDTO
    ) {
        agendaService.start(id, Optional.ofNullable(startSessionDTO).map(StartSessionDTO::getEndAt));
        return ResponseEntity.noContent().build();
    }
}
