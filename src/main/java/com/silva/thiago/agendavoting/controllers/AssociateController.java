package com.silva.thiago.agendavoting.controllers;

import com.silva.thiago.agendavoting.services.AssociateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "Api para consulta de associados")
@CrossOrigin(origins = "*")
@RequestMapping(AssociateController.BASE_URL)
public class AssociateController {
    public static final String BASE_URL = "/api/associate";
    private AssociateService associateService;

    public AssociateController(AssociateService associateService) {
        this.associateService = associateService;
    }

    @GetMapping
    @ApiOperation("Lista de associados")
    public ResponseEntity listAssociates() {
        return ResponseEntity.ok(associateService.findAll());
    }
}
