package com.silva.thiago.agendavoting.enums;

public enum DocumentStatusEnum {
    ABLE_TO_VOTE,
    UNABLE_TO_VOTE
}
