package com.silva.thiago.agendavoting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Agenda {
    @Id
    @SequenceGenerator(name="agenda_entity_seq", sequenceName = "agenda_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agenda_entity_seq")
    private Long id;
    private String title;
    private String description;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "begin_session_at")
    private LocalDateTime beginSessionAt;
    @Column(name = "end_session_at")
    private LocalDateTime endSessionAt;
    @OneToMany(mappedBy = "agenda", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Vote> voteList = new ArrayList<>();

    public Agenda(long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.voteList = new ArrayList<>();
    }

    @PrePersist
    public void prePersist() {
        this.createdAt = LocalDateTime.now();
    }

    public boolean hasOpenSession() {
        return this.beginSessionAt != null
            && this.endSessionAt != null
            && this.endSessionAt.isAfter(LocalDateTime.now());
    }
}
