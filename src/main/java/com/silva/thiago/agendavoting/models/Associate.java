package com.silva.thiago.agendavoting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Associate {
    @Id
    @SequenceGenerator(name = "associate_entity_seq", sequenceName = "associate_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "associate_entity_seq")
    private Long id;

    private String name;

    private String document;

    @OneToMany(mappedBy = "associate", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Vote> voteList = new ArrayList<>();
}
