package com.silva.thiago.agendavoting.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Vote {
    @Id
    @SequenceGenerator(name="vote_entity_seq", sequenceName = "vote_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vote_entity_seq")
    private Long id;

    @ManyToOne()
    @JoinColumn(name="associate_id", referencedColumnName = "id")
    private Associate associate;

    @ManyToOne()
    @JoinColumn(name="agenda_id", referencedColumnName = "id")
    private Agenda agenda;

    private Boolean option;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @PrePersist
    public void prePersist() {
        this.createdAt = LocalDateTime.now();
    }
}
