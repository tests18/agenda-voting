package com.silva.thiago.agendavoting.exceptions;

import com.silva.thiago.agendavoting.dtos.ErrorResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class MethodArgumentExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponseDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException ex, WebRequest request) {
        var result = ex.getBindingResult();
        Map<String, String> errors = result
            .getFieldErrors()
            .stream()
            .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));

        return new ErrorResponseDTO(
            HttpStatus.BAD_REQUEST.value(),
            "Validation failed.",
            errors
        );
    }
}
