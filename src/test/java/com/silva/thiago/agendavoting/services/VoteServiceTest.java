package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.CreateVoteDTO;
import com.silva.thiago.agendavoting.models.Agenda;
import com.silva.thiago.agendavoting.models.Associate;
import com.silva.thiago.agendavoting.models.Vote;
import com.silva.thiago.agendavoting.repositories.VoteRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class VoteServiceTest {
    private VoteRepository voteRepository;
    private AssociateService associateService;
    private DocumentCheckService documentCheckService;
    private VoteService voteService;


    @BeforeEach
    public void setUp() {
        voteRepository = Mockito.mock(VoteRepository.class);
        associateService = Mockito.mock(AssociateService.class);
        documentCheckService = Mockito.mock(DocumentCheckService.class);

        voteService = new VoteServiceImpl(voteRepository, associateService, documentCheckService);
    }

    @Test
    public void createVote() {
        var agenda = createFakeAgenda();
        var associate = createFakeAssociate();
        var createVoteDTo = new CreateVoteDTO(associate.getId(), true);

        when(associateService.findById(associate.getId())).thenReturn(associate);
        when(voteRepository.existsByAssociateIdAndAgendaId(associate.getId(), agenda.getId())).thenReturn(false);
        when(documentCheckService.isAbleToVote(associate.getDocument())).thenReturn(true);

        voteService.vote(agenda, createVoteDTo);

        verify(voteRepository, times(1)).save(Mockito.any(Vote.class));
    }

    @Test
    public void createVoteAtUninitializedAgenda() {
        var agenda = new Agenda(1L, "Agenda", "test");;
        var associate = createFakeAssociate();
        var createVoteDTo = new CreateVoteDTO(associate.getId(), true);
        try {
            voteService.vote(agenda, createVoteDTo);
        } catch (DataIntegrityViolationException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void createDuplicatedVote() {
        var agenda = createFakeAgenda();
        var associate = createFakeAssociate();
        var createVoteDTo = new CreateVoteDTO(associate.getId(), true);

        when(associateService.findById(associate.getId())).thenReturn(associate);
        when(voteRepository.existsByAssociateIdAndAgendaId(associate.getId(), agenda.getId())).thenReturn(true);
        try {
            voteService.vote(agenda, createVoteDTo);
        } catch (DataIntegrityViolationException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void associateUnableToVote() {
        var agenda = createFakeAgenda();
        var associate = createFakeAssociate();
        var createVoteDTo = new CreateVoteDTO(associate.getId(), true);

        when(associateService.findById(associate.getId())).thenReturn(associate);
        when(voteRepository.existsByAssociateIdAndAgendaId(associate.getId(), agenda.getId())).thenReturn(false);
        when(documentCheckService.isAbleToVote(associate.getDocument())).thenReturn(false);

        try {
            voteService.vote(agenda, createVoteDTo);
        } catch (DataIntegrityViolationException ex) {
            assertNotNull(ex);
        }
    }

    private Agenda createFakeAgenda() {
        var agenda = new Agenda(1L, "Agenda", "test");

        agenda.setBeginSessionAt(LocalDateTime.now());
        agenda.setEndSessionAt(LocalDateTime.now().plusMinutes(1L));

        return agenda;
    }

    private Associate createFakeAssociate() {
        return new Associate(1L, "John", "51198239042", new ArrayList<>());
    }
}
