package com.silva.thiago.agendavoting.services;

import com.silva.thiago.agendavoting.dtos.AgendaDTO;
import com.silva.thiago.agendavoting.dtos.AgendaResultDTO;
import com.silva.thiago.agendavoting.dtos.CreateAgendaDTO;
import com.silva.thiago.agendavoting.models.Agenda;
import com.silva.thiago.agendavoting.models.Associate;
import com.silva.thiago.agendavoting.models.Vote;
import com.silva.thiago.agendavoting.repositories.AgendaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.dao.DataIntegrityViolationException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

public class AgendaServiceTest {
    private ModelMapper mapper;
    private AgendaService agendaService;
    private AgendaRepository agendaRepository;

    @BeforeEach
    public void initTests() {
        mapper = Mockito.mock(ModelMapper.class);
        agendaRepository = Mockito.mock(AgendaRepository.class);
        this.agendaService = new AgendaServiceImpl(agendaRepository, mapper);
    }

    @Test
    public void createAgenda() {
        var createAgendaDTO = new CreateAgendaDTO("Agenda 1", "test agenda");
        var agenda = new Agenda(1L, "Agenda 1", "test agenda");
        var agendaDTO = new AgendaDTO(1L, "Agenda 1", "test agenda");
        when(mapper.map(createAgendaDTO, Agenda.class)).thenReturn(agenda);
        when(mapper.map(agenda, AgendaDTO.class)).thenReturn(agendaDTO);
        when(agendaRepository.save(Mockito.any(Agenda.class))).thenReturn(agenda);

        AgendaDTO result = agendaService.insert(createAgendaDTO);

        assertEquals(result, agendaDTO);
    }

    @Test
    public void startAgenda() {
        var agenda = createFakeAgenda();

        when(agendaRepository.getOne(agenda.getId())).thenReturn(agenda);
        when(agendaRepository.save(agenda)).thenReturn(agenda);
        agendaService.start(agenda.getId(), Optional.empty());

        assertNotNull(agenda.getBeginSessionAt());
        assertNotNull(agenda.getEndSessionAt());
    }

    @Test
    public void startWithCustomEndAt() {
        var agenda = createFakeAgenda();
        var endAt = LocalDateTime.now().plusMinutes(2);

        when(agendaRepository.getOne(agenda.getId())).thenReturn(agenda);
        when(agendaRepository.save(agenda)).thenReturn(agenda);
        agendaService.start(agenda.getId(), Optional.of(endAt));

        assertEquals(agenda.getEndSessionAt(), endAt);
    }

    @Test
    public void startAlreadyInitializedAgenda() {
        var agenda = this.createFakeAgenda();
        agenda.setBeginSessionAt(LocalDateTime.now());
        agenda.setEndSessionAt(LocalDateTime.now().plusMinutes(1));

        when(agendaRepository.getOne(agenda.getId())).thenReturn(agenda);
        when(agendaRepository.save(agenda)).thenReturn(agenda);

        try {
            agendaService.start(agenda.getId(), Optional.empty());
        } catch (DataIntegrityViolationException ex) {
            assertNotNull(ex);
        }
    }

    @Test
    public void results() {
        var agenda = createFakeAgenda();
        var associate1 = new Associate(1L, "A1", "77734816010", new ArrayList<>());
        var associate2 = new Associate(2L, "A2", "23804040039", new ArrayList<>());
        var associate3 = new Associate(3L, "A3", "51198239042", new ArrayList<>());
        var vote1 = new Vote(1L, associate1, agenda, true, LocalDateTime.now());
        var vote2 = new Vote(2L, associate2, agenda, true, LocalDateTime.now());
        var vote3 = new Vote(3L, associate3, agenda, false, LocalDateTime.now());

        agenda.getVoteList().add(vote1);
        agenda.getVoteList().add(vote2);
        agenda.getVoteList().add(vote3);

        when(agendaRepository.getOne(agenda.getId())).thenReturn(agenda);
        when(mapper.map(agenda, AgendaResultDTO.class)).thenCallRealMethod();

        AgendaResultDTO result = agendaService.result(agenda.getId());

        assertEquals(result.getAgendaTitle(), "Agenda 1");
        assertEquals(result.getPercentage().get(true), 66);
        assertEquals(result.getPercentage().get(false), 33);
        assertEquals(result.getResult().get(true), 2);
        assertEquals(result.getResult().get(false), 1);

    }

    private Agenda createFakeAgenda() {
        return new Agenda(1L, "Agenda 1", "test agenda");
    }
}
